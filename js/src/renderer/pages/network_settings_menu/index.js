// Copyright 2019 Assured Information Security, Inc. All Rights Reserved.

import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import NetworkSettingsMenu from '../../components/network_settings_menu';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <NetworkSettingsMenu/>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('app'));
