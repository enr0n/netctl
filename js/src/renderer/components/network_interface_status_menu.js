import React, {Component} from 'react';
import {ipcRenderer} from 'electron';

export default class NetworkInterfaceStatusMenu extends Component {
    constructor(props) {
        super(props);

        this.state = {
            interfaceName: '',
            interfaceIpAddress: ''
        };

        this.handleNetworkInterfaceUpdate = this.handleNetworkInterfaceUpdate.bind(this);
        this.renderNetworkInterfaceNotDetected = this.renderNetworkInterfaceNotDetected.bind(this);
        this.renderNetworkInterfaceTiles = this.renderNetworkInterfaceTiles.bind(this);

        ipcRenderer.on('update-network-interface', this.handleNetworkInterfaceUpdate);
    }

    handleNetworkInterfaceUpdate(event, arg) {
        this.setState(state => ({
            interfaceName: arg.interfaceName,
            interfaceIpAddress: arg.interfaceIpAddress
        }));
    }

    render() {
        if (this.state.interfaceName == '') {
            return this.renderNetworkInterfaceNotDetected();
        }
        
        return this.renderNetworkInterfaceTiles();
    }

    renderNetworkInterfaceNotDetected() {
        const component = (
            <div className={'network-settings-loading-message'}>
                Loading network settings...
            </div>
        );

        return component;
    }

    renderNetworkInterfaceTiles() {
        const interfaceNameLabel = 'Interface Name';
        const ipAddressLabel = 'IP Address';

        const component = (
            <div className={'network-interface-status-tile-container'}>
                <div className={'network-interface-name-tile'}>
                    <div className={'network-interface-tile-label'}>
                        {interfaceNameLabel}
                    </div>
                    <div className={'network-interface-tile-content'}>
                        {this.state.interfaceName}
                    </div>
                </div>
                <div className={'network-interface-ip-address-tile'}>
                    <div className={'network-interface-tile-label'}>
                        {ipAddressLabel}
                    </div>
                    <div className={'network-interface-tile-content'}>
                        {this.state.interfaceIpAddress}
                    </div>
                </div>
            </div>
        )

        return component;
    }
}
