const {app} = require('electron');
const path = require('path');
const process = require('process');

const WifiController = require('./controller/wifi');
const NetworkSettingsController = require('./controller/network_settings');
const AirplaneListener = require('./controller/airplane_listener');

function getSourceFileURL(filePath) {
    return `file://${path.join(__dirname, filePath)}`;
}

app.on('ready', () => {
    const netctlFrontendAddress = process.env.NETCTL_FRONTEND_ADDRESS;
    const netctlBackendAddress = process.env.NETCTL_BACKEND_ADDRESS;
    const wifiMenuIndex = getSourceFileURL('renderer/pages/wifi_menu/index.html');
    const networkSettingsIndex = getSourceFileURL('renderer/pages/network_settings_menu/index.html');
    const airplaneModeListener = new AirplaneListener(netctlFrontendAddress);

    wifiController = new WifiController(netctlFrontendAddress, wifiMenuIndex);
    networkSettingsController = new NetworkSettingsController(netctlFrontendAddress,
                                                              netctlBackendAddress,
                                                              networkSettingsIndex);
})
