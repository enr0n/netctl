// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package modem

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/godbus/dbus"
	"github.com/pkg/errors"
)

// State represents the modem connection state.
type State int32

// Possible values of State.
const (
	Unknown = iota
	Initializing
	Locked
	Disabled
	Disabling
	Enabling
	Enabled
	Searching
	Registered
	Disconnecting
	Connecting
	Connected

	Failed = -1
)

// String returns the string representation of State.
func (s State) String() string {
	switch s {
	case Failed:
		return "failed"
	case Unknown:
		return "unknown"
	case Initializing:
		return "initializing"
	case Locked:
		return "locked"
	case Disabled:
		return "disabled"
	case Disabling:
		return "disabling"
	case Enabling:
		return "enabling"
	case Enabled:
		return "enabled"
	case Searching:
		return "searching"
	case Registered:
		return "registered"
	case Disconnecting:
		return "disconnecting"
	case Connecting:
		return "connecting"
	case Connected:
		return "connected"
	}

	return "<none>"
}

// Manager is a wrapper around the ModemManager DBus interface.
type Manager struct {
	bus *dbus.Conn

	// Modem object and path
	mpath dbus.ObjectPath
	mobj  dbus.BusObject

	// Bearer object and path.
	bpath dbus.ObjectPath
	bobj  dbus.BusObject

	// Options supplied during connection, which are
	// used to create a bearer object.
	copts *connectOptions
}

// NewManager returns a new Manager.
func NewManager(modem string) (*Manager, error) {
	bus, err := dbus.SystemBus()
	if err != nil {
		return nil, err
	}

	mpath := dbus.ObjectPath(modem)

	obj := bus.Object("org.freedesktop.ModemManager1", mpath)

	m := &Manager{
		bus:   bus,
		mpath: mpath,
		mobj:  obj,
		copts: &connectOptions{},
	}

	return m, nil
}

// Close closes the Manager.
func (m *Manager) Close() error {
	if m.bus != nil {
		return m.bus.Close()
	}

	return nil
}

// ConnectOption is used to specify options during connection.
type ConnectOption interface {
	apply(*connectOptions)
}

type connectOptions struct {
	apn          string // Required
	user         string
	password     string
	allowRoaming *bool
}

type funcConnectOption struct {
	f func(*connectOptions)
}

func (fco *funcConnectOption) apply(c *connectOptions) {
	fco.f(c)
}

func newFuncConnectOption(f func(*connectOptions)) *funcConnectOption {
	return &funcConnectOption{f}
}

// WithUserAndPassword sets the user and password required by the network,
// if necessary.
func WithUserAndPassword(user, password string) ConnectOption {
	return newFuncConnectOption(func(c *connectOptions) {
		c.user = user
		c.password = password
	})
}

// WithAllowRoaming inidicates whether roaming should be allowed on the modem.
func WithAllowRoaming(allow bool) ConnectOption {
	return newFuncConnectOption(func(c *connectOptions) {
		c.allowRoaming = &allow
	})
}

// State returns the current state of the modem.
func (m *Manager) State() (State, error) {
	if m.mobj == nil {
		return Failed, errors.New("no modem managed")
	}

	v, err := m.mobj.GetProperty("org.freedesktop.ModemManager1.Modem.State")
	if err != nil {
		return Failed, err
	}

	if state, ok := v.Value().(int32); ok {
		return State(state), nil
	}

	return Failed, errors.Errorf("unexpected type %T for state", v.Value())
}

// CurrentAPN returns the APN that the modem is configured with.
func (m *Manager) CurrentAPN() string {
	if m.copts != nil {
		return m.copts.apn
	}

	return ""
}

// Connect connects the modem managed by Manager using the required
// APN, and additional ConnectOptions if supplied.
func (m *Manager) Connect(apn string, opts ...ConnectOption) error {
	if apn == "" {
		return errors.New("must specify APN for connection")
	}
	m.copts.apn = apn

	for _, opt := range opts {
		opt.apply(m.copts)
	}

	if err := m.createBearer(); err != nil {
		return errors.Wrap(err, "failed to create bearer")
	}

	if err := m.connectAndConfigureModem(); err != nil {
		return errors.Wrap(err, "failed to configure network")
	}

	return nil
}

func (m *Manager) deleteExistingBearers() error {
	bearers, err := m.mobj.GetProperty("org.freedesktop.ModemManager1.Modem.Bearers")
	if err != nil {
		return err
	}

	for _, b := range bearers.Value().([]dbus.ObjectPath) {
		call := m.mobj.Call("org.freedesktop.ModemManager1.Modem.DeleteBearer", 0, b)

		if err := call.Err; err != nil {
			return err
		}
	}

	return nil
}

func (m *Manager) useExistingBearer() error {
	v, err := m.mobj.GetProperty("org.freedesktop.ModemManager1.Modem.Bearers")
	if err != nil {
		return err
	}

	bearers, ok := v.Value().([]dbus.ObjectPath)
	if !ok {
		return errors.Errorf("got unexpected type %T for bearers list", v.Value())
	}

	// If there are multible bearers, only use the first one.
	if len(bearers) == 0 {
		return errors.New("no existing bearers")
	}

	m.bpath = bearers[0]
	m.bobj = m.bus.Object("org.freedesktop.ModemManager1", m.bpath)

	return nil
}

func (m *Manager) createBearer() error {
	// Some devices will only support one bearer at a time. For now, lazily delete any
	//existing bearers
	if err := m.deleteExistingBearers(); err != nil {
		log.Printf("Failed to delete existing bearers, connection attempt may be rejected.")
	}

	args := make(map[string]dbus.Variant)

	args["apn"] = dbus.MakeVariant(m.copts.apn)

	if user := m.copts.user; user != "" {
		args["user"] = dbus.MakeVariant(user)
	}

	if password := m.copts.password; password != "" {
		args["password"] = dbus.MakeVariant(password)
	}

	if m.copts.allowRoaming != nil {
		args["allow-roaming"] = dbus.MakeVariant(*m.copts.allowRoaming)
	}

	// If successful, we should get a new object path for a Bearer.
	call := m.mobj.Call("org.freedesktop.ModemManager1.Modem.CreateBearer", 0, args)

	if err := call.Store(&m.bpath); err != nil {
		m.bpath = ""

		return err
	}
	m.bobj = m.bus.Object("org.freedesktop.ModemManager1", m.bpath)

	return nil
}

func (m *Manager) connectAndConfigureModem() error {
	call := m.bobj.Call("org.freedesktop.ModemManager1.Bearer.Connect", 0)
	if err := call.Err; err != nil {
		return err
	}

	// We should now be able to get an IPv4 static
	// configuration from the Bearer.
	cfgv, err := m.bobj.GetProperty("org.freedesktop.ModemManager1.Bearer.Ip4Config")
	if err != nil {
		return errors.Wrap(err, "bearer has no IPv4 configuration")
	}

	cfg, ok := cfgv.Value().(map[string]dbus.Variant)
	if !ok {
		return errors.New("bad modem configuration format")
	}

	return m.generateSystemdNetworkConfig(cfg)
}

const (
	systemdNetworkTemplate = `# Auto-generated by netctl. Do not edit.
[Match]
Name=%v

[Network]
Address=%v/%v
Gateway=%v
DNS=%v
`
	systemdNetworkTemplateWithMTU = systemdNetworkTemplate + `
[Link]
MTUBytes=%v
`
)

func (m *Manager) generateSystemdNetworkConfig(cfg map[string]dbus.Variant) error {
	ifname, err := m.bobj.GetProperty("org.freedesktop.ModemManager1.Bearer.Interface")
	if err != nil {
		return errors.Wrap(err, "failed to get network interface name")
	}

	addr, ok := cfg["address"]
	if !ok {
		return errors.New("address is not specified")
	}

	prefix, ok := cfg["prefix"]
	if !ok {
		return errors.New("prefix is not specified")
	}

	gateway, ok := cfg["gateway"]
	if !ok {
		return errors.New("gateway is not specified")
	}

	dns := make([]string, 0)
	for i := 1; i <= 3; i++ {
		v, ok := cfg[fmt.Sprintf("dns%v", i)]
		if !ok {
			continue
		}

		dns = append(dns, v.Value().(string))
	}

	// Write the systemd network configuration.
	var out string

	in := []interface{}{
		ifname.Value(),
		addr.Value(),
		prefix.Value(),
		gateway.Value(),
		strings.Join(dns, " "),
	}

	// This might not be set, and that's OK.
	if mtu, ok := cfg["mtu"]; ok {
		in = append(in, mtu.Value())
		out = fmt.Sprintf(systemdNetworkTemplateWithMTU, in...)
	} else {
		out = fmt.Sprintf(systemdNetworkTemplate, in...)
	}

	dir := "/run/systemd/network"
	if err := os.MkdirAll(dir, 0755); err != nil {
		return err
	}

	path := filepath.Join(dir, fmt.Sprintf("10-%v-%v.network", ifname.Value(), m.copts.apn))

	if err := ioutil.WriteFile(path, []byte(out), 0644); err != nil {
		return err
	}

	cmd := exec.Command("networkctl", "reload")
	if out, err := cmd.CombinedOutput(); err != nil {
		return errors.Wrapf(err, "failed to restart systemd-networkd: %s", out)
	}

	return nil
}

// Disconnect disconnects the modem.
func (m *Manager) Disconnect() error {
	if m.bobj == nil {
		if err := m.useExistingBearer(); err != nil {
			return err
		}
	}

	call := m.bobj.Call("org.freedesktop.ModemManager1.Bearer.Disconnect", 0)

	return call.Err
}

// Enable enables the modem. The modem must be enabled before
// it can be connected.
func (m *Manager) Enable() error {
	return m.enableOrDisable(true /* enable */)
}

// Disable disables the modem.
func (m *Manager) Disable() error {
	return m.enableOrDisable(false /* disable */)
}

func (m *Manager) enableOrDisable(b bool) error {
	call := m.mobj.Call("org.freedesktop.ModemManager1.Modem.Enable", 0, b)

	return call.Err
}

// GetModems lists all modems currently available from the ModemManager
// DBus service.
func GetModems() ([]string, error) {
	var objs map[dbus.ObjectPath]map[string]map[string]dbus.Variant

	bus, err := dbus.SystemBus()
	if err != nil {
		return nil, err
	}

	// ModemManager implements the ObjectManager interface.
	// Get all available  Modem objects.
	obj := bus.Object("org.freedesktop.ModemManager1", "/org/freedesktop/ModemManager1")
	call := obj.Call("org.freedesktop.DBus.ObjectManager.GetManagedObjects", 0)

	if err := call.Store(&objs); err != nil {
		return nil, err
	}

	modems := make([]string, 0)
	for k := range objs {
		modems = append(modems, string(k))
	}

	return modems, nil
}
