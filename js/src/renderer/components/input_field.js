// Copyright 2019 Assured Information Security, Inc. All Rights Reserved.

import React,{Component} from 'react';

export default class InputField extends Component {
    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    onClick(event) {
        event.stopPropagation();
    }

    onChange(event) {
        this.props.onChange(event.target.value);
    }

    render() {
        let input = (
            <input
                autoFocus
                type={this.props.type ? this.props.type : 'text'}
                placeholder={this.props.placeholder ? this.props.placeholder : undefined}
                onChange={this.onChange}
                onClick={this.onClick}
                onKeyPress={this.props.onKeyPress}
                value={this.props.value}
            />
        );

        return (
            <div className='label-input'>
                {this.props.label}
                {input}
            </div>
        );
    }
}
