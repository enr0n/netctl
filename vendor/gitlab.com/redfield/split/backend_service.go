// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package split

import (
	"context"
	"log"
	"sync"

	"github.com/pkg/errors"
	"google.golang.org/grpc"

	"gitlab.com/redfield/split/internal/api"
	"gitlab.com/redfield/split/metadata"
)

// Backend implements the Back service.
type Backend struct {
	// This is now required by protoc-gen-go-grpc, in order
	// to guarantee forward compatibility when an RPC is added.
	api.UnimplementedBackendServer

	Metadata *metadata.BackendMetaData

	// Maintain a map of registered frontends, and
	// protect access to the map with a lock.
	mux       sync.Mutex
	frontends map[string]*metadata.FrontendMetaData

	// Handlers specified by backend implementations to allow
	// actions to happen on (un)registration.
	//
	// These should be protected by the mux as well.
	onRegisterFrontend   func(f *metadata.FrontendMetaData)
	onUnregisterFrontend func(f *metadata.FrontendMetaData)

	// gRPC server
	server *grpc.Server
}

// NewBackend returns a new Backend.
func NewBackend(addr string) (*Backend, error) {
	ti, err := ParseTransport(addr)
	if err != nil {
		return nil, errors.Wrap(err, "invald address")
	}

	b := &Backend{
		frontends: make(map[string]*metadata.FrontendMetaData),
		Metadata: &metadata.BackendMetaData{
			TransportInfo: ti.Info,
		},
	}

	return b, nil
}

// Serve starts the Backend server. It accepts variadic list of func's
// that accept a grpc.Server. This allows callers to supply a function
// to call in order to register their service on this Server.
//
// E.g., a caller might do:
//
//     f.Serve(func(s *grpc.Server) {
//             RegisterNetctlBackServer(s, myNetctlServer)
//     })
func (b *Backend) Serve(rfuncs ...func(s *grpc.Server)) error {
	t := &Transport{b.Metadata.TransportInfo}

	lis, err := t.Listener()
	if err != nil {
		return errors.Wrap(err, "failed to create listener")
	}
	defer lis.Close()

	b.server = grpc.NewServer()
	api.RegisterBackendServer(b.server, b)

	for _, register := range rfuncs {
		register(b.server)
	}

	return b.server.Serve(lis)
}

// Close tears down the Backend.
func (b *Backend) Close() error {
	if b.server != nil {
		b.server.Stop()
	}

	failedNotify := false

	for _, f := range b.getAllFrontends() {
		err := b.notifyBackendClosing(f)
		if err != nil {
			failedNotify = true
		}
	}

	if failedNotify {
		return errors.New("failed to clean up all frontends")
	}

	return nil
}

// SetRegisterHandler sets the function that should be called as a result
// of a frontend successfully registering with the backend.
func (b *Backend) SetRegisterHandler(handler func(f *metadata.FrontendMetaData)) {
	b.mux.Lock()
	defer b.mux.Unlock()

	b.onRegisterFrontend = handler
}

// SetUnregisterHandler sets the function that should be called as a result
// of a frontend unregistering with the backend.
func (b *Backend) SetUnregisterHandler(handler func(f *metadata.FrontendMetaData)) {
	b.mux.Lock()
	defer b.mux.Unlock()

	b.onUnregisterFrontend = handler
}

func (b *Backend) notifyBackendClosing(f *metadata.FrontendMetaData) error {
	fc, err := createFrontendClient(f)
	if err != nil {
		return errors.Wrap(err, "failed to create frontend client")
	}
	defer fc.Close()

	notification := &api.Notification{
		FrontUuid: f.Uuid,
		Type:      api.Notification_BACKEND_LEAVING,
	}
	err = fc.notify(notification)
	if err != nil {
		return errors.Wrap(err, "failed to notify frontend")
	}

	return nil
}

// RegisterFrontend registers a frontend with the backend.
func (b *Backend) RegisterFrontend(ctx context.Context, r *api.RegisterFrontendRequest) (*api.RegisterFrontendReply, error) {
	f := r.GetFrontend()
	if f == nil {
		return nil, errors.New("did not receive frontend information")
	}

	ti := f.GetTransportInfo()
	if ti == nil {
		return nil, errors.New("did not receive transport info for frontend")
	}

	err := b.initializeFrontend(f)
	if err != nil {
		return nil, errors.Wrap(err, "failed to initialize frontend")
	}

	log.Printf("%v frontend registered with UUID %v", f.Type, f.Uuid)

	b.mux.Lock()
	if b.onRegisterFrontend != nil {
		b.onRegisterFrontend(f)
	}
	b.mux.Unlock()

	return &api.RegisterFrontendReply{}, nil
}

func (b *Backend) initializeFrontend(f *metadata.FrontendMetaData) error {
	fc, err := createFrontendClient(f)
	if err != nil {
		return errors.Wrap(err, "failed to create frontend client")
	}
	defer fc.Close()

	notification := &api.Notification{
		FrontUuid: f.Uuid,
		Type:      api.Notification_REGISTER_ACK,
	}
	err = fc.notify(notification)
	if err != nil {
		return errors.Wrap(err, "failed to ACK frontend registration")
	}

	b.addFrontend(f)

	return nil
}

func createFrontendClient(f *metadata.FrontendMetaData) (*FrontendClient, error) {
	ti := &Transport{Info: f.TransportInfo}

	if ti.String() == "" {
		return nil, errors.New("transport unspecified")
	}

	cc, err := ti.ClientConn()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create client conn")
	}

	fc := NewFrontendClient(cc)

	return fc, nil
}

func (b *Backend) addFrontend(f *metadata.FrontendMetaData) {
	b.mux.Lock()
	defer b.mux.Unlock()

	b.frontends[f.Uuid] = f
}

func (b *Backend) getFrontend(uuid string) (*metadata.FrontendMetaData, error) {
	b.mux.Lock()
	defer b.mux.Unlock()

	f, ok := b.frontends[uuid]
	if !ok {
		return nil, errors.New("no such frontend")
	}

	return f, nil
}

func (b *Backend) getAllFrontends() []*metadata.FrontendMetaData {
	b.mux.Lock()
	defer b.mux.Unlock()

	frontends := make([]*metadata.FrontendMetaData, 0)

	for _, frontend := range b.frontends {
		frontends = append(frontends, frontend)
	}

	return frontends
}

func (b *Backend) removeFrontend(uuid string) {
	b.mux.Lock()
	defer b.mux.Unlock()

	delete(b.frontends, uuid)
}

// UnregisterFrontend unregisters a frontend from the backend.
func (b *Backend) UnregisterFrontend(ctx context.Context, r *api.UnregisterFrontendRequest) (*api.UnregisterFrontendReply, error) {
	f := r.GetFrontend()
	if f == nil {
		return nil, errors.New("did not receive frontend information")
	}

	err := b.teardownFrontend(f)
	if err != nil {
		return nil, errors.Wrap(err, "failed to teardown frontend")
	}

	log.Printf("%v frontend with UUID %v unregistered", f.Type, f.Uuid)

	b.mux.Lock()
	if b.onUnregisterFrontend != nil {
		b.onUnregisterFrontend(f)
	}
	b.mux.Unlock()

	return &api.UnregisterFrontendReply{}, nil
}

func (b *Backend) teardownFrontend(f *metadata.FrontendMetaData) error {
	_, err := b.getFrontend(f.Uuid)
	if err != nil {
		return err
	}

	b.removeFrontend(f.Uuid)

	return nil
}

// GetAllFrontends returns a list of registered frontends.
func (b *Backend) GetAllFrontends(ctx context.Context, r *api.GetAllFrontendsRequest) (*api.GetAllFrontendsReply, error) {
	frontends := &api.GetAllFrontendsReply{
		Frontends: b.getAllFrontends(),
	}

	return frontends, nil
}

// GetFrontend returns a frontend matching the specified UUID.
func (b *Backend) GetFrontend(ctx context.Context, r *api.GetFrontendRequest) (*api.GetFrontendReply, error) {
	f, err := b.getFrontend(r.GetUuid())
	if err != nil {
		return nil, errors.Wrap(err, "failed to find frontend")
	}

	frontend := &api.GetFrontendReply{
		Frontend: f,
	}

	return frontend, nil
}
