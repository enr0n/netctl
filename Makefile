GOPATH ?= $(HOME)/go
PATH := $(GOPATH)/bin:$(PATH)
PKGPATH ?= gitlab.com/redfield/toolstack
DESTDIR ?= /

PROTO_INCLUDE_DIR ?= /usr/local/include

.PHONY: all
all: bins

.PHONY: bins
bins:
	mkdir -p bin
	go build -o bin/netctl-back cmd/netctl-back/*.go
	go build -o bin/netctl-front cmd/netctl-front/*.go
	go build -o bin/netctl cmd/netctl/*.go

.PHONY: install
install: install-systemd-configs install-udev-rules
	install -d -m 0755 $(DESTDIR)/etc/dbus-1/system.d/
	install -m 0644 configs/com.gitlab.redfield.netctl.wireless.conf $(DESTDIR)/etc/dbus-1/system.d/
	install -d -m 0755 $(DESTDIR)/etc/bash_completion.d
	install -m 0755 configs/netctl.bash_completion.sh $(DESTDIR)/etc/bash_completion.d/

.PHONY: install-systemd-configs
install-systemd-configs:
	install -d -m 0755 $(DESTDIR)/lib/systemd/system/
	install -m 0644 configs/systemd/netctl-front@.service $(DESTDIR)/lib/systemd/system/netctl-front@.service
	install -m 0644 configs/systemd/netctl-back.service $(DESTDIR)/lib/systemd/system/netctl-back.service
	install -m 0644 configs/systemd/gen-netctl-front-env@.service $(DESTDIR)/lib/systemd/system/gen-netctl-front-env@.service
	install -m 0644 configs/systemd/gen-netctl-back-env.service $(DESTDIR)/lib/systemd/system/gen-netctl-back-env.service
	install -d -m 0755 $(DESTDIR)/usr/libexec
	install -m 0755 configs/systemd/gen-netctl-front-env $(DESTDIR)/usr/libexec/gen-netctl-front-env
	install -m 0755 configs/systemd/gen-netctl-back-env $(DESTDIR)/usr/libexec/gen-netctl-back-env

.PHONY: install-udev-rules
install-udev-rules:
	install -d -m 0755 $(DESTDIR)/lib/udev/rules.d/
	install -m 0644 configs/udev/70-netctl.rules $(DESTDIR)/lib/udev/rules.d/70-netctl.rules

.PHONY: clean
clean:
	rm -rf bin/

.PHONY: deps
deps:
	go get ./...


.PHONY: fmt
fmt:
	find api/ cmd/ pkg/ -pname '*.go' | xargs gofmt -w -s

.PHONY: vendor
vendor:
	GO111MODULE=on go mod vendor

api/%.pb.go: api/netctl.proto
	protoc -I api -I $(PROTO_INCLUDE_DIR)/split \
	    --go-grpc_out=api \
	    --go_out=api \
	    api/netctl.proto

.PHONY: proto
proto: api/netctl.pb.go api/netctl_grpc.pb.go

.PHONY: check
check: golint all test
	DESTDIR=/tmp make install

.PHONY: test
test:
	go test -v ./pkg/...

.PHONY: golint
golint:
	golangci-lint --verbose run --enable-all -Dgochecknoglobals -Dgochecknoinits -Dlll -Dfunlen
