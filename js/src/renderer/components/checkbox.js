import React from 'react';

const Checkbox = props => (
    <label
        className={'checkbox-container'}
        onClick={e => {e.stopPropagation();}}>
        <input type={'checkbox'}
            onClick={e => {e.stopPropagation();}}
            {...props}
        />
        <span className={'checkmark'}></span>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {props.label}
    </label>
)
export default Checkbox;
