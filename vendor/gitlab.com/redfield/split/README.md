# split - Split services for Redfield Toolstack

# Table of Contents
1. [Overview](#overview)
2. [Frontend-Backend Architecture](#frontend-backend-architecture)
   *  [Frontend Roles and Responsibilities](#frontend-roles-and-responsibilities)
   *  [Backend Roles and Responsibilities](#backend-roles-and-responsibilities)
3. [Getting Started](#getting-started)
   *  [Implementing Frontend Services](#implementing-frontend-services)
   *  [Implementing Backend Services](#implementing-backend-services)
   *  [Supported Transport Types](#supported-transport-types)

# Overview

The `split` package provides a framework for building "split" services, i.e. services that have split responsibilities between "frontend" and "backend" services.
The purpose of this service architecture is to enable services to be deployed in various configurations, across different virtual machines (VMs). Or, if needed,
all on one singal machine. One example of a service that uses this architecture is [`netctl`](https://gitlab.com/redfield/netctl), the networking toolstack for
Redfield.

This architecture relies on gRPC for IPC, with Go being the intended server-side implementation language. This has a few advantages:

1. The API is the same for local and remote management;
2. Many streaming transport protocols can be used, e.g. TCP, Unix domain sockets, Argo (Xen), etc.;
3. Provides easy inter-domain communication;
4. Clients can be written in many languages, e.g. Go, C++, JavaScript.

# Frontend-Backend Architecture

Some toolstack components, e.g. [netctl](#netctl), use an architecture that consists of **frontends** and **backends**. As such, these components provide two APIs; netctl-front and netctl-back in the case of netctl. This means that the service as a whole is disaggregated across two service definitions. The exact split will differ based on the service, but there are some basic ideas that apply to any service using this architecture. For example, services that use this architecture should have just one instance of a backend, but can have many instances of a frontend. This package provides the common pieces so that implementations only need to worry about the high-level responsibilities of their services.

## Frontend Roles and Responsibilities

At a high level, a frontend should provide an abstraction of something essential to the toolstack service. This abstraction will then shape the API and define the appropriate roles and responsibilities of the particular frontend implementation. For example, in netctl the abstraction of a frontend is a single network interface. Hence, the netctl API provides the ability to connect to wireless networks, and bring the interface up or down. Note that this means you could reasonably have multiple instances of `netctl-front` in a single VM.

Frontends provide the "core" of the toolstack service. When a client needs to perform some action like connect to a network, it should interface with the frontend responsible for the appropriate network interface.

## Backend Roles and Responsibilities

Unlike frontends, the role of a backend should be essentially the same across different toolstack services which use this architecture. Namely, a backend is responsible for tracking active frontends and managing their persistent data. When a frontend starts, it needs to know how to communicate with its backend. This is usually set as a command line option or environment variable. As its starting, the frontend registers itself with the backend, at which point the backend registers back with the frontend (this is essentially just a handshake). The backend then saves the metadata for the frontend (transport protocol, address, and any attributes necessary for the particular toolstack service). This fulfills the "tracking" responsibility of a backend. Clients such as UIs can then query the backend to get information about running frontends, and establish connections to frontends as needed.

Because Redfield service VMs have read-only filesystems, and are intended to be transient, frontends cannot rely on their host to save persistent data such as wireless network configurations. This is why the backend is responsible for this data; it is expected that backends run somewhere (such as the control domain) where persistent storage is available. Frontends may interface with the backend at any time to obtain this information, and typically grab saved configuration data as a post-initialization step.

As a consequence of these responsibilities, the backend serves not only client code and UIs, but the frontends themselves.

# Getting Started

To implement a service using split, the first step is to import `split/metadata/metadata.proto` to your `.proto` definition. This will allow the use of `FrontendMetaData` and `BackendMetaData`
types, an important parameter in split service APIs. To install the split proto files, run the following:

```bash
git clone git@gitlab.com:redfield/split.git
cd split
make install-proto
```

By default, this will install the proto files to `/usr/local/include/split`. Make sure to add `-I /usr/local/include` to your `protoc` flags.

When using Go, the metadata types are provided by `gitlab.com/redfield/split/metadata`. For JavaScript, you will need to re-generate a local copy
of `metadata_pb.js` along side your generated client code.

## Implementing Frontend Services

To start:

```go
import (
        "gitlab.com/redfield/split/metadata"
        "gitlab.com/redfield/split"
)
```

The `split.Frontend` type provides the API to a frontend in the split architecture. Create a new frontend
by providing the address which the frontend service should listen on:

```go
addr := "tcp://:50055"

f, err := split.NewFrontend(addr)
if err != nil {
        return err
}
```

To register your gRPC service on the frontend, call `Frontend.Serve`:

```go
f.Serve(func(s *grpc.Server) {
        // RegisterMyFrontendServer is a generated function for
        // the MyFrontend service.
        RegisterMyFrontendServer(s, myFrontend)
})
```

Once the server is running, it can be initialized with the backend, given the address
of the backend server:

```go
tr, err := split.ParseTransport(backendAddr)
if err != nil {
        return err
}

bmd := &metadata.BackendMetaData {
    TransportInfo: tr.Info,
}

f.Initialize(bmd, 3 * time.Second /* timeout */)
```

## Implementing Backend Services

Creating a backend is similar to a frontend, by using the `split.Backend` type.

```go
import (
        "gitlab.com/redfield/split/metadata"
        "gitlab.com/redfield/split"
)

addr := "tcp://:50056"

b, err := split.NewBackend(addr)
if err != nil {
        return err
}

b.Serve(func(s *grpc.Server) {
        // RegisterMyBackendServer is a generated function for
        // the MyBackend service.
        RegisterMyBackendServer(s, myBackend)
})
```

At this point, frontends will be able to `Initialize`, or register, with the backend service.

## Supported Transport Types

Creating a `split.Transport` is easiest with the `split.ParseTransport` function. Currently, the following transport types are supported:

1. IPv4
2. IPv6
3. Unix Domain Sockets
4. Argo (Xen)

The `Transport.ClientConn` function is a convenience for creating a `grpc.ClientConn` from a `split.Transport`. Likewise, `Transport.Listener` creates
a `net.Listener` from the `split.Transport`.
