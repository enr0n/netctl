import React, {Component} from 'react';

import NetworkList from './network_list';
import NetworkSettingsButton from './network_settings_button';

export default class WifiMenu extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        const component = (
            <div className={'wifi-menu'}>
                <NetworkList key="network-list"/>
                <NetworkSettingsButton key="network-settings-button"/>
            </div>
        );

        return component;
    }
}
