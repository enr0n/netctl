const webpack = require('webpack');

module.exports = {
    target: 'electron-renderer',
    entry: {
        wifi_menu: './src/renderer/pages/wifi_menu/index.js',
        network_settings_menu: './src/renderer/pages/network_settings_menu/index.js'
    },

    output: {
        path: __dirname + '/src/renderer/pages',
        filename: '[name]/[name]_bundle.js'
    },

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/react']
                }
            },
            {
                test: /\.svg$/,
                use: ['@svgr/webpack']
            }
        ]
    },

    resolve: {
      extensions: ['.js', '.json', '.jsx']
    }
}
