import React, {Component} from 'react';
import {ipcRenderer} from 'electron';

import NetworkInterfaceStatusMenu from './network_interface_status_menu';
import SavedNetworksList from './saved_networks_list';

export default class NetworkSettingsMenu extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        ipcRenderer.send('network-settings-menu-ready', null);
    }

    render() {
        const component = (
            <div className={'network-settings-menu'}>
                <NetworkInterfaceStatusMenu key="network-interface-status-menu"/>
                <SavedNetworksList key="saved-networks-list"/>
            </div>
        );

        return component;
    }
}
