NETCTL_ROOT ?= ../

NPM_FLAGS ?= --runtime=electron --target=7.0.0
NPM_BIN = $(NETCTL_ROOT)/js/node_modules/.bin

MODE ?= development

PATH := $(PATH):$(NPM_BIN)

PROTO_INCLUDE_DIR ?= /usr/local/include

.PHONY: all
all: npm-install proto webpack

.PHONY: npm-install
npm-install:
	npm install $(NPM_FLAGS)

src/api/netctl_grpc_pb.js src/api/netctl_pb.js: $(NETCTL_ROOT)/api/netctl.proto
	grpc_tools_node_protoc \
	    -I $(NETCTL_ROOT)/api -I $(PROTO_INCLUDE_DIR)/split \
	    --js_out=import_style=commonjs,binary:src/api \
	    --grpc_out=src/api \
	    --plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin` \
	    netctl.proto

# Unlike Go, grpc-node does not seem to have any support for re-using generated
# code by specifying a package that provides it (Go has `option go_package` in .proto).
#
# For now it seems we need to re-gen a local copy to resolve the generated require
# statements.
src/api/metadata/metadata_pb.js:
	grpc_tools_node_protoc -I $(PROTO_INCLUDE_DIR)/split \
	    --js_out=import_style=commonjs,binary:src/api \
	    metadata/metadata.proto

src/api/split/split_pb.js src/api/split/split_grpc_pb.js:
	grpc_tools_node_protoc -I $(PROTO_INCLUDE_DIR) -I $(PROTO_INCLUDE_DIR)/split \
	    --js_out=import_style=commonjs,binary:src/api \
	    --grpc_out=src/api \
	    --plugin=protoc-gen-grpc=`which grpc_tools_node_protoc_plugin` \
	    split/split.proto

.PHONY: proto
proto: src/api/netctl_grpc_pb.js src/api/netctl_pb.js src/api/metadata/metadata_pb.js \
    src/api/split/split_grpc_pb.js src/api/split/split_pb.js

.PHONY: dist-windows
dist-windows:
	electron-builder --win

.PHONY: webpack
webpack:
	webpack --config webpack.dev.config.js --mode=$(MODE)

.PHONY: clean
clean:
	rm -rf node_modules dist

.PHONY: clean-proto
clean-proto:
	rm -f $(NETCTL_ROOT)/js/src/api/*pb.js
	rm -rf $(NETCTL_ROOT)/js/src/api/split
	rm -rf $(NETCTL_ROOT)/js/src/api/metadata

.PHONY: jslint
jslint:
	find src/ -name '*.js' | xargs ./node_modules/.bin/eslint -c .eslintrc.json
