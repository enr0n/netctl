// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package frontend

import (
	"context"
	"log"
	"time"

	"github.com/pkg/errors"
	"google.golang.org/grpc"

	"gitlab.com/redfield/split"
	"gitlab.com/redfield/split/metadata"

	"gitlab.com/redfield/netctl/api"
	"gitlab.com/redfield/netctl/pkg/modem"
)

// ModemFrontend is a netctl frontend that is linked to a modem.
type ModemFrontend struct {
	// This is now required by protoc-gen-go-grpc, in order
	// to guarantee forward compatibility when an RPC is added.
	api.UnimplementedNetctlFrontServer

	frontend *split.Frontend
	mm       *modem.Manager

	bc *api.BackendClient
}

// NewModemFrontend returns a new netctl frontend for a modem.
func NewModemFrontend(mpath string, addr string) (*ModemFrontend, error) {
	frontend, err := split.NewFrontend(addr)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create frontend")
	}

	uuid, err := hashInterfaceNameWithUUID("modem")
	if err != nil {
		return nil, errors.Wrap(err, "failed to generate uuid for frontend")
	}

	frontend.Metadata.Uuid = uuid
	frontend.Metadata.Type = api.InterfaceType_MODEM.String()

	mm, err := modem.NewManager(mpath)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create modem manager")
	}

	if err := mm.Enable(); err != nil {
		return nil, errors.Wrap(err, "failed to enable modem")
	}

	mf := &ModemFrontend{
		frontend: frontend,
		mm:       mm,
	}

	return mf, nil
}

// Initialize starts the initialization phase and performs startup actions.
func (mf *ModemFrontend) Initialize(nb *metadata.BackendMetaData, timeout time.Duration) error {
	err := mf.frontend.Initialize(nb, timeout)
	if err != nil {
		return err
	}

	t := &split.Transport{
		Info: nb.TransportInfo,
	}

	cc, err := t.ClientConn()
	if err != nil {
		return errors.Wrap(err, "failed to create backend client after initialization")
	}
	mf.bc = api.NewBackendClient(cc)

	mf.postInitialization()

	return nil
}

// Serve starts the wireless frontend service.
func (mf *ModemFrontend) Serve() error {
	return mf.frontend.Serve(func(s *grpc.Server) {
		api.RegisterNetctlFrontServer(s, mf)
	})
}

// Close tears down the ModemFrontend
func (mf *ModemFrontend) Close() error {
	if mf.mm != nil {
		_ = mf.mm.Disable()

		if err := mf.mm.Close(); err != nil {
			return err
		}
	}

	if mf.bc != nil {
		if err := mf.bc.Close(); err != nil {
			return errors.Wrap(err, "failed to close backend client")
		}
	}

	return mf.frontend.Close()
}

// postInitialization performs any actions that should happen as
// a part of service startup, but need to wait until the frontend
// has initialized with the backend.
func (mf *ModemFrontend) postInitialization() {
	go mf.connectWithSavedBearerConfig()
}

func (mf *ModemFrontend) ModemConnect(ctx context.Context, r *api.ModemConnectRequest) (*api.ModemConnectReply, error) {
	bopts := r.GetBearerCfg()

	if err := mf.doConnect(bopts); err != nil {
		return nil, err
	}

	mf.saveBearerConfig(bopts)

	return &api.ModemConnectReply{}, nil
}

func (mf *ModemFrontend) doConnect(cfg *api.BearerConfiguration) error {
	copts := make([]modem.ConnectOption, 0)

	apn := cfg.GetApn()
	if apn == "" {
		return errors.New("must specify APN for connection")
	}

	user, password := cfg.User, cfg.Password
	if user != "" && password != "" {
		copts = append(copts, modem.WithUserAndPassword(user, password))
	}

	if cfg.OverrideDefaultRoaming {
		copts = append(copts, modem.WithAllowRoaming(cfg.AllowRoaming))
	}

	if err := mf.mm.Connect(apn, copts...); err != nil {
		return err
	}

	return nil
}

func (mf *ModemFrontend) connectWithSavedBearerConfig() {
	nf := mf.frontend.Metadata

	confs, err := mf.bc.GetSavedBearerConfs(nf)
	if err != nil {
		log.Print(errors.Wrap(err, "failed to get saved bearer configurations"))
		return
	}

	// Lazily, just use the first one that works. In practice, there
	// should really only be one.
	for _, conf := range confs {
		if err := mf.doConnect(conf); err != nil {
			log.Printf("Failed to connect with saved configuration for %v", conf.GetApn())
		} else {
			log.Printf("Connected with saved configuration for %v", conf.GetApn())

			// Connection was successful, no need to try any more.
			break
		}
	}
}

func (mf *ModemFrontend) saveBearerConfig(conf *api.BearerConfiguration) {
	err := mf.bc.SaveBearerConf(mf.frontend.Metadata, conf)
	if err != nil {
		log.Printf("Failed to save bearer configuration for '%v'", conf.GetApn())
	}
}

func (mf *ModemFrontend) ModemDisconnect(ctx context.Context, r *api.ModemDisconnectRequest) (*api.ModemDisconnectReply, error) {
	if err := mf.mm.Disconnect(); err != nil {
		return nil, err
	}

	return &api.ModemDisconnectReply{}, nil
}

func (mf *ModemFrontend) ModemGetProperties(ctx context.Context, r *api.ModemGetPropertiesRequest) (*api.ModemGetPropertiesReply, error) {
	state, err := mf.mm.State()
	if err != nil {
		return nil, err
	}

	props := &api.ModemProperties{
		State: state.String(),
		Apn:   mf.mm.CurrentAPN(),
	}

	return &api.ModemGetPropertiesReply{Props: props}, nil
}
