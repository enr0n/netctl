// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package argo

import (
	"encoding/binary"
	"fmt"
	"unsafe"

	"golang.org/x/sys/unix"
)

// In lieu of support for an equivalent to _IOW et al in
// golang.org/x/sys/unix, re-create the macro(s) needed
// according to:
// https://elixir.bootlin.com/linux/latest/source/include/uapi/asm-generic/ioctl.h
const (
	iocNRBits   = 8
	iocTypeBits = 8
	iocSizeBits = 14
	iocDirBits

	iocNRShift   = 0
	iocTypeShift = iocNRShift + iocNRBits
	iocSizeShift = iocTypeShift + iocTypeBits
	iocDirShift  = iocSizeShift + iocSizeBits

	iocNRMask   = (1 << iocNRBits) - 1
	iocTypeMask = (1 << iocTypeBits) - 1
	iocSizeMask = (1 << iocSizeBits) - 1
	iocDirMask  = (1 << iocDirBits) - 1
)

func ioc(dir, typ, nr, size uint) uint {
	return ((dir & iocDirMask) << iocDirShift) |
		((typ & iocTypeMask) << iocTypeShift) |
		((nr & iocNRMask) << iocNRShift) |
		((size & iocSizeMask) << iocSizeShift)
}

func iow(typ, nr, size uint) uint {
	return ioc(1 /* Write */, typ, nr, size)
}

const (
	ioctlArgo = uint('W')
)

var (
	iocBind    = iow(ioctlArgo, 2, sizeofRingID)
	iocConnect = iow(ioctlArgo, 5, sizeofXenAddr)
	iocListen  = iow(ioctlArgo, 7, 4 /* uint32 */)
	iocAccept  = iow(ioctlArgo, 8, sizeofXenAddr)
)

func ioctl(fd uintptr, req uint, arg []byte) (uintptr, error) {
	trap := uintptr(unix.SYS_IOCTL)
	r, _, e := unix.Syscall(trap, fd, uintptr(req), uintptr(unsafe.Pointer(&arg[0])))
	if e != 0 {
		return 0, e
	}

	return r, nil
}

const (
	// For clarity, express sizes as the sum of the
	// size of each struct field.
	sizeofXenAddr = 4 + 2 + 2
	sizeofRingID  = 2 + 2 + 4
)

type ringID struct {
	domid   uint16
	partner uint16
	port    uint32
}

func (rid *ringID) bytes() []byte {
	buf := make([]byte, sizeofRingID)

	binary.LittleEndian.PutUint16(buf[0:], rid.domid)
	binary.LittleEndian.PutUint16(buf[2:], rid.partner)
	binary.LittleEndian.PutUint32(buf[4:], rid.port)

	return buf
}

// This function is not currently needed, but if it becomes
// needed, it can just be uncommented.
//
// func (rid *ringID) decode(buf []byte) {
//	rid.domid = binary.LittleEndian.Uint16(buf[0:])
// 	rid.partner = binary.LittleEndian.Uint16(buf[2:])
// 	rid.port = binary.LittleEndian.Uint32(buf[4:])
// }

type XenAddr struct {
	Port  uint32
	Domid uint16
	_     uint16 // pad
}

func (xa *XenAddr) bytes() []byte {
	buf := make([]byte, sizeofXenAddr)

	binary.LittleEndian.PutUint32(buf[0:], xa.Port)
	binary.LittleEndian.PutUint16(buf[4:], xa.Domid)

	return buf
}

func (xa *XenAddr) decode(buf []byte) {
	xa.Port = binary.LittleEndian.Uint32(buf[0:])
	xa.Domid = binary.LittleEndian.Uint16(buf[4:])
}

func (xa *XenAddr) Network() string {
	return "argo"
}

func (xa *XenAddr) String() string {
	return fmt.Sprintf("%d:%d", xa.Domid, xa.Port)
}
