// Copyright 2020 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package split

import (
	"context"
	"time"

	"google.golang.org/grpc"

	"gitlab.com/redfield/split/internal/api"
	"gitlab.com/redfield/split/metadata"
)

// BackendClient provides a client for the Backend service.
type BackendClient struct {
	c api.BackendClient

	// Keep track so it can be closed later.
	cc *grpc.ClientConn

	// Timeout for RPC calls
	timeout time.Duration
}

// NewBackendClient returns a new Backend client.
func NewBackendClient(cc *grpc.ClientConn) *BackendClient {
	bc := &BackendClient{
		c:       api.NewBackendClient(cc),
		cc:      cc,
		timeout: 30 * time.Second,
	}

	return bc
}

// SetTimeout sets the timeout used to call non-streaming RPCs.
func (bc *BackendClient) SetTimeout(timeout time.Duration) {
	bc.timeout = timeout
}

// Close closes the backend client connection.
func (bc *BackendClient) Close() error {
	return bc.cc.Close()
}

// RegisterFrontend registers a frontend with the backend.
func (bc *BackendClient) RegisterFrontend(nf *metadata.FrontendMetaData) error {
	r := &api.RegisterFrontendRequest{
		Frontend: nf,
	}

	ctx, cancel := context.WithTimeout(context.Background(), bc.timeout)
	defer cancel()

	_, err := bc.c.RegisterFrontend(ctx, r)

	return err
}

// UnregisterFrontend unregisters a frontend with the backend.
func (bc *BackendClient) UnregisterFrontend(nf *metadata.FrontendMetaData) error {
	r := &api.UnregisterFrontendRequest{
		Frontend: nf,
	}

	ctx, cancel := context.WithTimeout(context.Background(), bc.timeout)
	defer cancel()

	_, err := bc.c.UnregisterFrontend(ctx, r)

	return err
}

// GetAllFrontends returns a list of registered frontends.
func (bc *BackendClient) GetAllFrontends() ([]*metadata.FrontendMetaData, error) {
	r := &api.GetAllFrontendsRequest{}

	ctx, cancel := context.WithTimeout(context.Background(), bc.timeout)
	defer cancel()

	reply, err := bc.c.GetAllFrontends(ctx, r)

	return reply.GetFrontends(), err
}

// GetFrontend returns a frontend matching the specified UUID, if any.
func (bc *BackendClient) GetFrontend(uuid string) (*metadata.FrontendMetaData, error) {
	r := &api.GetFrontendRequest{
		Uuid: uuid,
	}

	ctx, cancel := context.WithTimeout(context.Background(), bc.timeout)
	defer cancel()

	reply, err := bc.c.GetFrontend(ctx, r)

	return reply.GetFrontend(), err
}
