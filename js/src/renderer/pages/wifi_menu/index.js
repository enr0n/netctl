// Copyright 2019 Assured Information Security, Inc. All Rights Reserved.

import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import WifiMenu from '../../components/wifi_menu';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <WifiMenu/>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('app'));
